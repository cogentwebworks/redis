## very small redis container but still have a working s6-overlay process and socklog-overlay


[![Docker Pulls](https://img.shields.io/docker/pulls/cogentwebs/redis.svg)](https://hub.docker.com/r/cogentwebs/redis/)
[![Docker Stars](https://img.shields.io/docker/stars/cogentwebs/redis.svg)](https://hub.docker.com/r/cogentwebs/redis/)
[![Docker Build](https://img.shields.io/docker/automated/cogentwebs/redis.svg)](https://hub.docker.com/r/cogentwebs/redis/)
[![Docker Build Status](https://img.shields.io/docker/build/cogentwebs/redis.svg)](https://hub.docker.com/r/cogentwebs/redis/)

This is a very small redis container but still have a working s6-overlay process and socklog-overlay .